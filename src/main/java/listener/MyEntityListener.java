package listener;
import javax.persistence.PrePersist;
import entity.User;

public class MyEntityListener {
	
	@PrePersist
	public void prePersist(User user) {
		System.out.println("prePersist() listener");
	}
}
