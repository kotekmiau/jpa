import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.apache.log4j.Logger;
import org.eclipse.persistence.indirection.IndirectSet;
import entity.Group;
import entity.User;
import entity.UserRole;

public class Main {
	
	Logger log = Logger.getLogger(Main.class);
			
	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");
		EntityManager em = emf.createEntityManager();

		/**
		 * start transaction
		 */
		em.getTransaction().begin();
		
		Group group = em.find(Group.class, 1L);
		if(group == null) {
			group = new Group("group1");
		}
		
		User user = new User(UUID.randomUUID().toString());
		user.setGroup(group);
		user.setRole(UserRole.values()[new Random().nextInt(3)]);
		em.persist(user);
		em.persist(group);
		
		/**
		 * commit
		 */
		em.getTransaction().commit();
		
		Query q = em.createQuery("select u from User u");
		for(Object o: q.getResultList()) {
			log.info(o);
		}

		/**
		 * lazy loading
		 */
		Group g1 = em.find(Group.class, 1L);
		log.info(g1);
		IndirectSet s = (IndirectSet) g1.getUsers();
		log.info(s.isInstantiated());
		log.info(g1.getUsers().size());	// force load
		log.info(g1.getUsers());
		log.info(s.isInstantiated());
		
		/**
		 * use named query
		 */
		Query nq = em.createNamedQuery("User.findAll");
		List<User> l = nq.getResultList();
		log.info(l);
		

	}

}
